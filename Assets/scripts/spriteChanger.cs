﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spriteChanger : MonoBehaviour {

    public SpriteRenderer sRender;
    public Transform tf;
    public Behaviour behaviour;
    public float MoveSpeed = 0.125f;

	// Use this for initialization
	void Start () {
        sRender = GetComponent<SpriteRenderer> ();
        tf = GetComponent<Transform>();
        behaviour = GetComponent<Behaviour>();
	}

    // Update is called once per frame
    void Update() {
        if (Input.GetKey(KeyCode.LeftShift)) {
            if (Input.GetKeyDown(KeyCode.RightArrow)) {
                //move to the right one unit when both are pressed
                tf.position += Vector3.right;
            }
        }
        else if (Input.GetKey(KeyCode.RightArrow)) {
            //move to the right at a constant speed
            tf.position += Vector3.right * MoveSpeed;
        }
        if (Input.GetKey(KeyCode.LeftShift)) {
            if (Input.GetKeyDown(KeyCode.LeftArrow)) {
                //move to the left one unit when both are pressed
                tf.position += Vector3.left;
            }
        }
        else if (Input.GetKey(KeyCode.LeftArrow)) {
            //move to the left ar a constant speed
            tf.position += Vector3.left * MoveSpeed;
        }
        if (Input.GetKey(KeyCode.LeftShift)) {
            if (Input.GetKeyDown(KeyCode.UpArrow)) {
                //move up one unit when both are pressed
                tf.position += Vector3.up;
            }
        }
        else if(Input.GetKey(KeyCode.UpArrow)) {
            //move up at a constant speed
            tf.position += Vector3.up * MoveSpeed;
        }
        if (Input.GetKey(KeyCode.LeftShift)) {
            if (Input.GetKeyDown(KeyCode.DownArrow)) {
                //move down one unit when both are pressed
                tf.position += Vector3.down;
            }
        }
        else if (Input.GetKey(KeyCode.DownArrow)) {
            //move down at a constant speed
            tf.position += Vector3.down * MoveSpeed;
        }
        if (Input.GetKey(KeyCode.Space)) {
            //sets the sprite back to the center when space is pressed
           tf.position = Vector3.zero ;
        }
        if (Input.GetKey(KeyCode.Q)){
            //this breaks the GameObject, which shuts down the whole GameObject, making everything disappear.
            gameObject.SetActive(false);
        }
        if (Input.GetKey(KeyCode.Escape)) {
            //if the game were running in an actual build, pressing escape would quit the game
            Application.Quit();
        }
    }
}
