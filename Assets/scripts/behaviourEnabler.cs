﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class behaviourEnabler : MonoBehaviour {

    public Behaviour behaviour;

    // Use this for initialization
    void Start()
    {
        behaviour = GetComponent<Behaviour>();
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.P))
        {
            //activates and deactivates the ability to move
            behaviour.enabled = !behaviour.enabled;
        }
    }
}
